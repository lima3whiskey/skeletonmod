package com.fireball1725.skeletonmod.item;

import com.fireball1725.skeletonmod.reference.ModInfo;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;

public enum Items {
    // REGISTER ITEMS HERE
    ;

    private static boolean registered = false;
    public final Item item;
    private final String internalName;

    Items(String internalName, Item item) {
        this(internalName, item, null);
    }

    Items(String internalName, Item item, CreativeTabs creativeTabs) {
        this.internalName = internalName;
        this.item = item.setTextureName(ModInfo.MOD_ID + ":" + internalName);
        item.setUnlocalizedName(ModInfo.MOD_ID + "." + internalName);
        item.setCreativeTab(creativeTabs);
    }

    public static void registerAll() {
        if (registered)
            return;
        for (Items i : Items.values())
            i.register();
        registered = true;
    }

    private void register() {
        GameRegistry.registerItem(item, internalName);
    }

    public String getStatName() {
        return StatCollector.translateToLocal(item.getUnlocalizedName());
    }

    public ItemStack getStack(int damage, int size) {
        return new ItemStack(item, size, damage);
    }
}
