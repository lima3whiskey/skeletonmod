package com.fireball1725.skeletonmod.util;

import cpw.mods.fml.common.FMLCommonHandler;

public class Platform {
    public static boolean isServer() {
        return FMLCommonHandler.instance().getEffectiveSide().isServer();
    }

    public static boolean isClient() {
        return FMLCommonHandler.instance().getEffectiveSide().isClient();
    }
}
