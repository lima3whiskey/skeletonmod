package com.fireball1725.skeletonmod.proxy;

public interface IProxy {
    public abstract void registerBlocks();
    public abstract void registerItems();
}
